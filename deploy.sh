#!/usr/bin/env bash


./node_modules/.bin/vite build
scp -P 15601 -r dist/* root@47.111.131.42:/usr/local/apps/apps/website_navigation/
rm -fr dist/
exit 0
